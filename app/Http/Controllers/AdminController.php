<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Auth;
use Inertia\Inertia;
use App\Models\User;
use App\Models\Car;
use App\Models\CarPhoto;

class AdminController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');        
    }

    public function index() 
    {
        $users = User::all();
        $cars = Car::with(['carphotos','user'])->get();
        return Inertia::render('Users/Dashboard', compact('users','cars'));
    }

    public function deleteUser(User $user)
    {
        foreach($user->cars as $car){
            foreach($car->carphotos as $carphoto ){
                //Storage::delete('public/images/'.$carphoto->image);
                (new CarPhotoDelete())->delete($carphoto);
            }
        } 

        $user->delete();

        return Redirect::route('users');        
        
    }
}
