<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Car;
use App\Models\CarPhoto;
use App\Models\User;
use App\Http\Controllers\CarController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Cart\CartController;
use Illuminate\Support\Facades\URL;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function (Request $request) {
    $cars = Car::with(['carphotos','user'])->when($request->brand, function($query, $brand){                
        return $query->where('brand','LIKE','%'.$brand.'%');
    })
    ->when($request->fromyear, function($query, $fromyear){
        return $query->where('year', '>=' ,$fromyear);
    })  
    ->when($request->toyear, function($query, $toyear){
        return $query->where('year', '<=' ,$toyear);
    })  
    ->when($request->pricefrom, function($query, $pricefrom){
        return $query->where('price', '>=' ,$pricefrom);
    })  
    ->when($request->priceto, function($query, $priceto){
        return $query->where('price', '<=' ,$priceto);
    })        
    ->when($request->kmfrom, function($query, $kmfrom){
        return $query->where('km', '>=' ,$kmfrom);
    })  
    ->when($request->kmto, function($query, $kmto){
        return $query->where('km', '<=' ,$kmto);
    })
    ->when($request->gearbox, function($query, $gearbox){                
        return $query->where('gearbox','LIKE','%'.$gearbox.'%');
    })
    ->when($request->emissions, function($query, $emissions){                
        return $query->where('emissions','LIKE','%'.$emissions.'%');
    })    
    ->when($request->service, function($query, $service){                
        return $query->where('service','LIKE','%'.$service.'%');
    })  
    ->when($request->filter=='desc', function($query, $filter){              
        return $query->orderBy('price', 'DESC');
    }) 
    ->when($request->filter=='asc', function($query, $filter){              
        return $query->orderBy('price', 'ASC');
    })
    ->when($request->filter=='newest', function($query, $filter){              
        return $query->orderBy('created_at', 'DESC');
    })
    ->when($request->filter=='oldest', function($query, $filter){              
        return $query->orderBy('created_at', 'ASC');
    })

    ->paginate(3)->withQueryString()->through(function ($car) { 

        return [
            'id' => $car->id,                
            'brand' => $car->brand,
            'price' => $car->price,
            'km' => $car->km,
            'gearbox' => $car->gearbox,
            'year' => $car->year,
            'images' => $car->carphotos->map(function($carphoto) {                                
                            $carphoto->image = asset('/storage/images/'.$carphoto->image);
                            return $carphoto;
                        }),        
            'showurl' => URL::route('cars.show', $car->id)           
        ];
    });
    //dd($cars);
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'cars' =>  $cars,
        'filters' =>  $request->only(['brand','fromyear','toyear','pricefrom', 'priceto', 'kmfrom', 'kmto','gearbox','emissions','service','filter'])             
    ]);
    
})->name('/cars');



Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/cart', [CartController::class, 'index'])->name('cart.index');
Route::post('/cart', [CartController::class, 'store'])->name('cart.store');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');    
});

Route::middleware('admin')->group(function() {
    Route::get('/users', [AdminController::class, 'index'])->name('users');
    Route::delete('/deleteuser/{user}',[AdminController::class, 'deleteUser'])->name('deleteuser');
});

Route::middleware('user')->group(function() {
    Route::get('/mycars', [CarController::class, 'mycars'])->name('mycars');
    Route::delete('/destroypicture/{carphoto}',[CarController::class, 'destroyPicture'])->name('destroypicture');
    Route::post('/updatecar/{car}', [CarController::class, 'updateCar'])->name('updatecar');
});

Route::resource('cars', CarController::class);

require __DIR__.'/auth.php';
