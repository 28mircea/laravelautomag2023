<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>{{ config('app.name', 'AutoMag') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />
        
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="frontend/assets/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="frontend/assets/css/style.css">
        <!-- Scripts -->
        <!-- jQuery -->
        <!--<script src="{{asset('frontend/assets/js/jquery-2.1.0.min.js')}}"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

        <!-- Bootstrap -->
        <script src="{{asset('frontend/assets/js/popper.js')}}"></script>
        <script src="{{asset('frontend/assets/js/bootstrap.min.js')}}"></script>       

        <!-- Plugins -->
        <script src="{{asset('frontend/assets/js/scrollreveal.min.js')}}"></script>
        <script src="{{asset('frontend/assets/js/waypoints.min.js')}}"></script>
        <script src="{{asset('frontend/assets/js/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('frontend/assets/js/imgfix.min.js')}}"></script> 
        <script src="{{asset('frontend/assets/js/mixitup.js')}}"></script> 
        <script src="{{asset('frontend/assets/js/accordions.js')}}"></script>

        <!-- Global Init -->
        <script src="{{asset('frontend/assets/js/custom.js')}}"></script>
        @routes
        @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
        @inertiaHead
    </head>
    <body class="font-sans antialiased">
        @inertia
    </body>
</html>
